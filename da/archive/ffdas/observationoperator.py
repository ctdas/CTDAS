#!/usr/bin/env python
# stilt_tools.py

"""
Author : W. He
Adapted by Ingrid Super, last revisions 14-8-2018
Adapted by Auke van der Woude, 09-2019 -- ...

Revision History:
| -- Basing on Wouter's codes, replace the TM5 model with the STILT model, April 2015.
| -- Expanding code to include C14 and a biosphere module 
This module holds specific functions needed to use the STILT model within the data assimilation shell. It uses the information
from the DA system in combination with the generic stilt.rc files.

The STILT model is now controlled by a python subprocess. This subprocess consists of an MPI wrapper (written in C) that spawns
a large number ( N= nmembers) of STILT model instances under mpirun, and waits for them all to finish.

#The design of the system assumes that the stilt function are executed using R codes, and is residing in a
#directory specified by the ${RUNDIR} of a stilt rc-file. This stilt rc-file name is taken from the data assimilation rc-file. Thus,

"""
import shutil
import os
import sys
import logging
import datetime as dtm
import numpy as np
from numpy import array, logical_and
import glob
import da.tools.io4 as io
import subprocess
import netCDF4 as nc
import da.tools.rc as rc
from da.tools.general import create_dirs, to_datetime
from da.baseclasses.observationoperator import ObservationOperator

try: # Import memoization. This speeds up the functions a lot.
    from memoization import cached
except: # The memoization package is not always included. Import the memoization from functools
    from functools import lru_cache as cached
    cached = cached()
import xlrd


from da.ffdas import category_info
categories = category_info.categories
# global constants, which will be used in the following classes
identifier = 'WRF-STILT'
version = '1.0'

### Constants for the calculation of fluxes
MASS_C              = 12.00
MASS_C_14           = 14.00
MASS_CO             = 28.01                                                          # kg/kmol
MASS_CO2            = 44.01                                                          # kg/kmol
MASS_14CO2          = 46.00                                                          # kg/kmol
Aabs             = 226.                                                           # Bq/kgC         Absolute radiocarbon standard (Mook and van der Plicht, 1999)
dt_obs           = 0.                                                             # s              time between sampling and measuring
d13C             = -9.                                                            # per mille
Nav              = 6.023e23                                                       # molecules/mole Avogadro's number
tau_14CO2        = 5700 * 365*24*3600.                                            # s              half life time of 14CO2
lambda_14CO2     = np.log(2)/tau_14CO2                                            # s-1            radioactive decay

MOL2UMOL = 1.0e6
KMOL2UMOL= 1.0e9
PERHR2PERS = 1.0/(3600.)
PERYR2PERS = 1.0/(365.*24.*3600.)
epsilon_14CO2_gpp = -36.                                                         # per mille
alpha_14CO2_gpp   = 1 + epsilon_14CO2_gpp/1000.                                  # = 0.964
Rstd            = 1.36e-12                                                       # kg 14C / kgC     # 4.48e-12                                  #  kg 14CO2/kgC (according to Fabian)
delta_14CO2_bg   =    44.6                                                        # per mille
delta_14CO2_ff   = -1000.0                                                        # per mille
delta_14CO2_bf   =  -200.0                                                        # per mille (2*-18 per mille for C3 plants, email dd 28/05/2019)
delta_14CO2_nuc  =  0.7e15                                                        # per mille

R_14CO2_ff = (delta_14CO2_ff /1000. + 1) * Rstd                                 # scalar 0.0
R_14CO2_bf = (delta_14CO2_bf /1000. + 1) * Rstd                                 # scalar 1.501e-12
R_14CO2_bg = (delta_14CO2_bg /1000. + 1) * Rstd                                 # scalar 1.577e-12
epsilon_14CO2_gpp = -36.                                                         # per mille
alpha_14CO2_gpp   = 1 + epsilon_14CO2_gpp/1000.                                  # = 0.964
################### Begin Class STILT ###################


DO_RINGO = 0
class STILTObservationOperator(ObservationOperator):
    def __init__(self, filename, dacycle=None):   #only the filename used to specify the location of the stavector file for wrf-stilt runs
        """ The instance of an STILTObservationOperator is application dependent """
        self.ID = identifier    # the identifier gives the model name
        self.version = version       # the model version used
        self.restart_filelist = []
        self.output_filelist = []
        self.outputdir = None # Needed for opening the samples.nc files created

        logging.info('Observation Operator initialized: %s (%s)' % (self.ID, self.version))

        self.load_rc(filename)   # load the specified rc-file
        self.filename = filename 
        if dacycle != None:
            self.dacycle = dacycle
        else:
            self.dacycle = {}

        self.startdate=None

        if DO_RINGO: logging.warning('DO_RINGO is used, so the time indices +=1')


    def setup(self, dacycle):
        """ Execute all steps needed to prepare the ObsOperator for use inside CTDAS,
         only done at the very first cycle normally """
        self.dacycle = dacycle
        self.outputdir = dacycle['dir.output']
        self.obsoperator_rc = dacycle['da.obsoperator.rc'] #stilt_footprint.rc
        self.obsfile = dacycle.dasystem['obs.input.id']
        self.obsdir = dacycle.dasystem['datadir']
        self.nrloc = int(dacycle.dasystem['obs.input.nr'])
        self.nrspc = int(dacycle.dasystem['obs.spec.nr'])
        self.nrcat = int(dacycle.dasystem['obs.cat.nr'])
        self.bgswitch = int(dacycle.dasystem['obs.bgswitch'])
        self.bgfile = dacycle.dasystem['obs.background']
        self.btime = int(dacycle.dasystem['run.backtime'])
        self.categories = categories
        self.paramdict = rc.read('/home/awoude/ffdas/RINGO/Data/paramdict.rc')

    def load_rc(self, name):
        """This method loads a STILT rc-file with settings for this simulation
        Args:
            name: str: name of the .rc file that will be loaded
                Should include: - num_backtimes
                                - files_startdate
                                - footdir
                                - datadir
        Returns
            None
        """
        
        self.rcfile = rc.RcFile(name)
        self.model_settings = rc.read(name)
        logging.debug('rc-file loaded successfully')

    def prepare_run(self,do_pseudo,adv):
        """ Prepare the running of the actual forecast model, for example compile code
        1) Set general parameters (name of outfile, number of members)
        2) Parse the input obsfile (self.obsfile)
        3) Add the data from the obsfile to the ObservationOperator instance
        4) Find out what model will be used for which emissions
        Args:
            do_pseudo: bool:
                0 if ensemble run needs to be done
                1 if pseudo_observations are used 
        Returns:
            None"""

        # Define the name of the file that will contain the modeled output of each observation
        if adv==0:
            self.simulated_file = os.path.join(self.outputdir,
                                               'samples_simulated.%s.nc' % self.dacycle['time.sample.stamp'])
        else:
            self.simulated_file = os.path.join(self.outputdir,
                                               'samples_simulated.%s.adv.nc' % self.dacycle['time.sample.stamp'])
        
        # Set the number of ensemble members
        self.forecast_nmembers = int(self.dacycle['da.optimizer.nmembers'])
        # Set the inputdir
        self.param_inputdir = self.dacycle['dir.input']
        
        #list all observation locations and species
        self.lat = []
        self.lon = []
        self.hgt = []
        self.obsnc = []
        self.mmass = []
        self.sitenames=[]
        self.recalc_factors = []
        infile = os.path.join(self.obsdir, self.obsfile)    
        f = open(infile, 'r')
        lines = f.readlines()
        f.close()
        self.spname = []
        
        # Parse input file
        for line in lines:
            if line[0] == '#': continue
            else:
                ct_, filename, lat, lon, height, species_name, species_mass, recalc_factor, *_ = line.split(',')
                two_names = any(x in filename for x in ['UW', 'DW'])
                sitename = ('_'.join(filename.split('_')[1:2 + two_names]))
                ct = int(ct_) - 1 # Set the counter
                self.obsnc.append(filename)
                self.sitenames.append(sitename)
                if species_name in self.spname:
                    if species_name == self.spname[0]:
                        self.lat.append(lat)
                        self.lon.append(lon)
                        self.hgt.append(height)
                else:
                    self.spname.append(species_name)
                    self.mmass.append(species_mass)
                    self.recalc_factors.append(recalc_factor)
                    if ct == 0:
                        self.lat.append(lat)
                        self.lon.append(lon)
                        self.hgt.append(height)
                    
        self.temporal_var = []
        for k, v in self.categories.items():
            self.temporal_var.append(v['temporal'])
        #set time control variables for this cycle
        if do_pseudo==0:
            self.timestartkey = dtm.datetime(self.dacycle['time.sample.start'].year,
                                             self.dacycle['time.sample.start'].month,
                                             self.dacycle['time.sample.start'].day,
                                             self.dacycle['time.sample.start'].hour, 0)
            self.timefinishkey = dtm.datetime(self.dacycle['time.sample.end'].year,
                                              self.dacycle['time.sample.end'].month,
                                              self.dacycle['time.sample.end'].day,
                                              self.dacycle['time.sample.end'].hour, 0)
        elif do_pseudo==1:
            self.timestartkey = dtm.datetime(self.dacycle['time.fxstart'].year,
                                             self.dacycle['time.fxstart'].month,
                                             self.dacycle['time.fxstart'].day,
                                             self.dacycle['time.fxstart'].hour, 0)
            self.timefinishkey = dtm.datetime(self.dacycle['time.finish'].year,
                                              self.dacycle['time.finish'].month,
                                              self.dacycle['time.finish'].day,
                                              self.dacycle['time.finish'].hour, 0)
        self.get_c14_time_profile()

    def get_samples(self, samples):
        self.samples = samples.datalist
        datelist = [sample.xdate for sample in self.samples]
        self.datelist = sorted(datelist)
        logging.info('Added samples to the observation operator')

    def get_c14_time_profile(self):
        """Function that loads the nuclear power temporal variation"""
        # Get the time profiles of the nuclear plants
        temp_distribution_file = self.model_settings['nuclear_timeprofiledir']
        workbook      = xlrd.open_workbook(temp_distribution_file)
        worksheet     = workbook.sheet_by_name('Sheet1')
        time_profile_nuc = np.array(worksheet.col_values(19))
        self.nuc_time_profile = time_profile_nuc

    ### !!!! Only cache if made dependent on datepoint/datetime !!!!
    def get_time_index_nc(self, time=None):
        """Function that gets the time index from the flux files
        based on the cycletime and the first time in all the files (hardcoded in stilt.rc)
        Input:
            time: datetime.datetime: The time for which the index needs to be found. Default: current time cycle datetime
        """
        if time == None:
            # Get the time of the current cycle
            time = self.dacycle['time.start']
        # Get the start date of all cycles
        startdate = self.model_settings['files_startdate']
        startdate = dtm.datetime.strptime(startdate, '%Y-%m-%d %H:%M:%S')
        # Get the difference between the current and the start
        # Note that this is in hours, and thus assumes that the flux files are hourly as well
        timediff = time - startdate
        timediff_hours = int(timediff.total_seconds()/3600) # 1hour could also be softcoded from time.cycle
        time_index = int(timediff_hours) + DO_RINGO
        return time_index
    
    def get_time_indices(self, datepoint):
        """Function that gets the time indices in the flux files
        Because if the footprint is for 24 hours back, we need the fluxes 24 hours back"""

        time_index = self.get_time_index_nc(datepoint)
        return slice(time_index - int(self.model_settings['num_backtimes']), time_index)

    @cached
    def get_foot(self, site,  datepoint):
        """Function that gets the footprint for the current time and site.
        Returns a 3D np.array with dims (time, lat, lon)
        Input:
            i_loc: number of location. Requires object to have a 'sitenames' property
            datepoint: datetime of the footprint to be found.
        Returns:
            np.array (3d): Footprint with as indices time, lat, lon
            """

        path = self.model_settings['footdir'] + '/' + site.upper() + '24'
        fname = path + '/footprint_{0}_{1}x{2:02d}x{3:02d}x{4:02d}*.nc'.format(site.upper(),
                                                                               datepoint.year,
                                                                               datepoint.month,
                                                                               datepoint.day,
                                                                               datepoint.hour)
        f = glob.glob(fname)[0]
        footprint = nc.Dataset(f)['foot']

        # Flip, because the times are negative
        return np.flipud(footprint)

    @cached 
    def get_background(self, i_species, site, datepoint):
        """Function that finds the center of mass of the first footprint and the time corresponding to it.
            and finds the concentration in the center of mass. This is used as the background.
        Input: 
            i_species: int: the index of the species for which the background should be found
            i_loc    : int: the index of the locatin for which the background should be found
            datepoint: datetime.datetime: the datetime of the background concentration
        Returns:
            float: background concentration
            """
        from scipy import ndimage
        
        # First, get the footprint and find the first time of influence
        foot = self.get_foot(site, datepoint)
        start_influence = -1 # In backtimes
        total_influence = 0
        while total_influence < 0.0000001:
            start_influence += 1
            total_influence = fp[start_influence].sum()
        
        # Get the center of mass of the first influence
        center_of_mass = ndimage.measurements.center_of_mass(fp[start_influence])
        center_of_mass = np.array(np.rint(center_of_mass), dtype=int)

        species_name = self.spname[i_species]

        index  = self.get_time_index_nc() - (int(self.model_settings['num_backtimes']) - start_influence)
        
        # Find the concentration at the center of mass at the correct time.
        background_file = self.model_settings['bgfdir'] + '/background.nc'
        background_map  = nc.Dataset(background_file)[species_name]
        background_conc = background_map[index, center_of_mass[0], center_of_mass[1]]
        
        return background_conc
    
    def get_background_orig(self, i_species):
        """Function that finds the background concentration, non-time dependent and hard-coded.
        Input:
            i_species: int: the index of the species for which the background should be found
            i_loc    : int: the index of the locatin for which the background should be found
            datepoint: datetime for which the background concentration should be found.
        Returns:
            float: background concentration
            """

        backgrounds = 406.15, 127.2
        return backgrounds[i_species]

    @cached
    def get_c14_concentration(self, site, datepoint,  gpp, ter, ff_flux, background):
        """Function that gets the c14 concentration based on the emissions by the biosphere, 
            fossil fuels and nuclear power. The constants are defined at the top of this script.
        Input: 
            i_loc: int: the index of the location for which the c14 concentration should be calculated
            datepoint: datepoint: datetime for which the c14 concentration should be calculated
            gpp: float: The gross primary production in umol/s
            ter: float: The total ecosystem respiration in umol/s
            ff_flux: float: The fossil fuel flux in umol/s
            background: float: The background CO2 concentration
        Returns: 
            float: The C14 in ppm
            float: Delta(14C) """
        # First, get the footprint
        foot = self.get_foot(site, datepoint)
        # Get the time indices
        indices = self.get_time_indices(datepoint)
        
        # Get the fluxes from the nuclear plants
        nuclear_time_profile = self.nuc_time_profile[indices]
        # Get the fluxes and multiply them by the footprint and time profile
        # Note that the unit is already in Delta notation
        file = self.model_settings['nuclear_fluxdir']
        nuc_flux = self.get_nc_variable(file, 'E_14CO2_nuc')
        nuc_flux = (nuclear_time_profile[:, None, None] * nuc_flux[None,:] * foot).sum()

        # now get the disequilibrium flux
        file = self.model_settings['biosphere_fluxdir']
        dis_eq_flux = self.get_nc_variable(file, 'TER_dC14')
        delta_14CO2_ter = (dis_eq_flux[indices] * foot).sum()
        R_14CO2_ter = (delta_14CO2_ter/1000. + 1) * Rstd # [nt,nlat,nlon] 1.639e-12 [1.557e-12 - 1.787e-12]

        # Now, calculate to ppm
        co2_14_ff = ff_flux   * R_14CO2_ff * MASS_C / MASS_C_14
        co2_14_bg = background* R_14CO2_bg * MASS_C / MASS_C_14
        co2_14_total = co2_14_ff + (alpha_14CO2_gpp * R_14CO2_bg * gpp) + (R_14CO2_ter * ter) + nuc_flux + co2_14_bg

        co2_total = gpp + ter + ff_flux + background


#        print('ff', co2_14_ff * 1E12)
#        print('bg', co2_14_bg * 1E12)
#        print('gpp', alpha_14CO2_gpp * R_14CO2_bg * gpp * 1E12)
#        print('ter', R_14CO2_ter * ter)
#        print('nuc', nuc_flux)
        # Conversion 14CO2 in ppm to DELTA ----
        Rm           = (co2_14_total * MASS_14CO2) / (co2_total * MASS_CO2)   # ppm to mass ratio (kg 14CO2 / kgCO2)
        A14          = Rm * lambda_14CO2 * 1e3*Nav / MASS_14CO2 # kg14CO2/kgCO2 * 1/s * molecules/kmole / (kg14CO2/kmole14CO2) = molecules 14CO2 / kgCO2 / s 
        As           = A14 * MASS_CO2 / MASS_C # Bq/kgC-CO2
        Asn = As * ( 1 - 2 * (25+d13C)/1000.)  # Bq/kgC-CO2
        delta_14CO2 = (Asn * np.exp(lambda_14CO2 * dt_obs) / Aabs -1) * 1000.# per mille
        return delta_14CO2
#        d_obsco2_obs = delta_bg * background + delta_bio * biosphere_flux + delta_ff * ff_flux + delta_nuc * nuc_flux
#        co2_obs = background + biosphere_flux + ff_flux + nuc_flux
#        d_obs = d_obsco2_obs/co2_obs
#        return d_obs

    @cached
    def get_nc_variable(self, file, fluxname):
        """Helper function that gets the values from an nc file
        Input: 
            file: str: filename of the nc file of which the value should be taken
            fluxname: str: name of the variable in the file
        Returns:
            np.ndarray: the values in the nc file"""
        data = nc.Dataset(file, 'r')
        fluxmap = data[fluxname][:]
        data.close()
        return fluxmap
    
    @cached
    def get_biosphere_concentration(self,site, datepoint, total=False):
        """Function that calculates the atmospheric increase due to the exchange of fluxes over the footprint
        Input:
            i_loc: int: index of the location for which the concentration should be calculated
            datepoint: datetime.datetime: the datepoint for which the concentration should be calculated
            total: bool, optional: Whether to returned summed values or gpp and ter seperately as tuple
        Returns:
            if total == True:
                float: The total biosphere flux in umol/s
            else: 
                tuple of 2 floats: GPP and TER in umol/s """

        # First, get the footprint
        foot = self.get_foot(site, datepoint)
        # Get the indices 
        indices = self.get_time_indices(datepoint)
        # Get the tracer fluxes
        file = self.model_settings['biosphere_fluxdir']
        gpp_flux = self.get_nc_variable(file, 'GPP')[indices] * (1./MASS_C) * MOL2UMOL * PERHR2PERS
        ter_flux = self.get_nc_variable(file, 'TER')[indices] * (1./MASS_C) * MOL2UMOL * PERHR2PERS
        gpp_increase = - (gpp_flux * foot).sum()
        ter_increase = (ter_flux * foot).sum()

#        logging.debug('GGP flux = {0:.3}; TER flux = {1:.3}'.format(gpp_increase, ter_increase)) 
        if total: return gpp_increase + ter_increase
        else: return gpp_increase, ter_increase

    @cached
    def get_temporal_profiles(self, datepoint, station_name,  i_member):
        """ Function that reads in the temporal profiles for the current timestep
        Input:
            datepoint: datepoint: datetime for which the temporal profile should be found
            i_station: int: the index of the location for which the c14 concentration should be calculated
            i_member: int: the index of the member for which the simulation is run
        Returns:
            np.array (2): the time profiles for all categories. Indices: time, category"""
        #read temporal profiles for the times within the footprint
        time_indices = self.get_time_indices(datepoint)
        temporal_prior = os.path.join(self.model_settings['datadir'],'prior_temporal_{0}_{1:03}.nc'.format(station_name, i_member))
        temporal_prior = io.ct_read(temporal_prior, 'read')
        temp_profiles = []
        for category, values in self.categories.items():
            temporal_var_name = values['temporal']
            temporal_variation = temporal_prior.get_variable(temporal_var_name)[time_indices]
            temp_profiles.append(temporal_variation)
        #temporal_prior.close()
        temp_profiles = np.array(temp_profiles)
        self.temp_profiles = np.asarray(temp_profiles)
        return temp_profiles.T # Transpose to be similar to spatial data in dimensions

    @cached
    def get_spatial_emissions(self, i_member, station,  i_species):
        """ Function that gets the spatial emissions
        Input:
            i_member: int: the index of the member for which the simulation is run
            i_species: int: the index of the species for which the simulation is run
        Returns:
            np.ndarray (3d): the spatial emissions per category, lat and lon"""
        #read spatially distributed emissions calculated with the emission model
        emisfile = os.path.join(self.model_settings['datadir'],'prior_spatial_{0}_{1:03d}.nc'.format(station, i_member)) #format: species[SNAP,lon,lat]
        self.emisfile = emisfile
        f = io.ct_read(emisfile,method='read')
        emis=f.get_variable(self.spname[i_species])
        f.close()
        return emis

    @cached
    def run_STILT(self, datepoint, i_member, site, i_species, do_pseudo=0):
        """This function reads in STILT footprints and returns hourly concentration data
        Input:
            datepoint: datepoint: datetime: the datepoint for which the concentrations should be calculated
            i_member   : int: index of the ensemblemember
            i_loc      : int: index of the location
            i_species  : int: index of the species
            do_pseudo  : bool: whether to do pseudo-observations or not
        Returns:
            float: the concentration increase at the respective location due to the emissions from the respective species"""
        # get the date:
        temp_profiles = self.get_temporal_profiles(datepoint, site,  i_member)
        spatial_emissions = self.get_spatial_emissions(i_member, site, i_species)
        #find correct background concentrationa
        footprint = self.get_foot(site, datepoint)

        # multiply footprint with emissions for each hour in the back trajectory. Indices: Time, Category, lat, lon
        foot_flux = (footprint[:, None, :, :] * spatial_emissions[None, :, :, :] * temp_profiles[:, :, None, None]).sum()
        concentration_increase = float(self.recalc_factors[i_species]) * foot_flux
 
        return concentration_increase

    def run_forecast_model(self,dacycle,do_pseudo,adv):
        if do_pseudo==0:
            self.prepare_run(do_pseudo,adv)
           # self.validate_input()
            self.run(dacycle,do_pseudo)
            self.save_data()
        elif do_pseudo==1:
            self.prepare_run(do_pseudo,adv)
            self.run(dacycle,do_pseudo)
            self.save_obs()
        self.run_breakdown()

    def run(self,dacycle,do_pseudo):
        """Function that loops over all members, locations, species and datepoints and calculates the concentration increase at that point (in time)
        Adds the data as attribute to this object."""

        totser=np.array(int(self.dacycle['da.optimizer.nmembers'])*[len(self.datelist)*[0.]])
        self.totser = totser
        for i_member in range(int(self.dacycle['da.optimizer.nmembers'])):
            logging.debug('Calculating concentration for member {}'.format(i_member))
            conc_STILT=[]
            for sample in self.samples:
                if sample.flag == 99: conc_STILT.append(np.nan)
                else:
                    species = sample.species; i_species = self.spname.index(species.upper())
                    datepoint = sample.xdate
                    site = sample.code.split('/')[-1].split('_')[1]
                    logging.debug('working on site {} time {}'.format(site, datepoint))
                    background_concentration = self.get_background_orig(i_species)
                    conc_increase_stilt = self.run_STILT(datepoint, i_member, site, i_species)
                    if species.upper() == 'CO2':
                        gpp, ter = self.get_biosphere_concentration(site, datepoint)
                        logging.debug('{0:.3} for FF and {1:.3} for bio for {2}'.format(conc_increase_stilt, gpp+ter, self.spname[i_species]))
                    else:
                        gpp, ter = 0, 0
                        logging.debug('{0:.3} for CO '.format(conc_increase_stilt))
                    conc_STILT.append(conc_increase_stilt + gpp + ter + background_concentration)
            totser[i_member] = np.array(conc_STILT)
            logging.info('Concentration calculated for member {}'.format(i_member))
        self.mod = np.array(totser).T
        logging.debug('Finished doing simulations per back trajectory time')

    def run_breakdown(self):
        """Function that clears cache for all functions that are cached
        so that next time, run will make new forecasts

        commented out functions are cached, but not optimised and do 
        therefore not need an empty cache."""
        self.run_STILT.cache_clear()
        self.get_spatial_emissions.cache_clear()
        self.get_nc_variable.cache_clear()
        # self.get_biosphere_concentration.cache_clear()
        # self.get_background.cache_clear()
        # self.self.get_background_orig.cache_clear()
        self.get_c14_concentration.cache_clear()
        # self.get_foot.cache.clear()
        self.get_temporal_profiles.cache_clear()

    def save_data(self):
        """ Write the data that is needed for a restart or recovery of the Observation Operator to the save directory """

        import da.tools.io4 as io
        
        f = io.CT_CDF(self.simulated_file, method='create')
        logging.debug('Creating new simulated observation file in ObservationOperator (%s)' % self.simulated_file)

        dimid = f.createDimension('obs_num', size=None)
        dimid = ('obs_num',)
        savedict = io.std_savedict.copy() 
        savedict['name'] = "obs_num"
        savedict['dtype'] = "int"
        savedict['long_name'] = "Unique_Dataset_observation_index_number"
        savedict['units'] = ""
        savedict['dims'] = dimid
        savedict['comment'] = "Unique index number within this dataset ranging from 0 to UNLIMITED."
        f.add_data(savedict,nsets=0)

        dimmember = f.createDimension('nmembers', size=self.forecast_nmembers)
        dimmember = ('nmembers',)
        savedict = io.std_savedict.copy() 
        savedict['name'] = "model"
        savedict['dtype'] = "float"
        savedict['long_name'] = "mole_fraction_of_trace_gas_in_air"
        savedict['units'] = "mol tracer (mol air)^-1"
        savedict['dims'] = dimid + dimmember
        savedict['comment'] = "Simulated model value"
        f.add_data(savedict,nsets=0)
        
        f_in = io.ct_read(self.dacycle['ObsOperator.inputfile'],method='read') 
        ids = f_in.get_variable('obs_num')
        
        for i,data in enumerate(zip(ids,self.mod)):
            f.variables['obs_num'][i] = data[0]
            f.variables['model'][i,:] = data[1]
            dum=f.variables['model'][:]
        f.close()
        f_in.close()
        
    def save_obs(self):
        """Write pseudo-observations to file"""

        ct1=0
        for k in range(self.nrloc*self.nrspc):
            newfile = os.path.join(self.obsdir,self.obsnc[k]+'.nc')
            f = io.CT_CDF(newfile, method='create')
            logging.debug('Creating new pseudo observation file in ObservationOperator (%s)' % newfile)
            
            dimid = f.add_dim('Time',len(self.datelist))
            ln = len(self.datelist)
            str19 = f.add_dim('strlen',19)
            str3 = f.add_dim('strlen2',3)

            data=[]
            for it,t in enumerate(self.datelist):
                data.append(list(t.isoformat().replace('T','_')))
            
            savedict = io.std_savedict.copy() 
            savedict['dtype'] = "char"
            savedict['name'] = "Times"
            savedict['units'] = ""
            savedict['dims'] = dimid + str19
            savedict['values'] = data
            f.add_data(savedict)
            
            data = ln*[self.lat[int(k/self.nrspc)]]
            savedict = io.std_savedict.copy() 
            savedict['name'] = "lat"
            savedict['units'] = "degrees_north"
            savedict['dims'] = dimid
            savedict['values'] = data
            f.add_data(savedict)
            
            data = ln*[self.lon[int(k/self.nrspc)]]
            savedict = io.std_savedict.copy() 
            savedict['name'] = "lon"
            savedict['units'] = "degrees_east"
            savedict['dims'] = dimid
            savedict['values'] = data
            f.add_data(savedict)
            
            data = ln*[self.hgt[int(k/self.nrspc)]]
            savedict = io.std_savedict.copy() 
            savedict['name'] = "alt"
            savedict['units'] = "m above ground"
            savedict['dims'] = dimid
            savedict['values'] = data
            f.add_data(savedict)
            
            savedict = io.std_savedict.copy() 
            savedict['name'] = "obs"
            savedict['units'] = "ppm or ppb"
            savedict['dims'] = dimid
            savedict['values'] = self.mod_prior[ct1:ct1+ln]
            f.add_data(savedict)
            
            savedict = io.std_savedict.copy() 
            savedict['dtype'] = "char"
            savedict['name'] = "species"
            savedict['units'] = "observed species"
            savedict['dims'] = dimid + str3
            savedict['values'] = self.sps[ct1:ct1+ln]
            f.add_data(savedict)
                
                
            savedict = io.std_savedict.copy() 
            savedict['dtype'] = "int"
            savedict['name'] = "ids"
            savedict['units'] = "unique observation identification number"
            savedict['dims'] = dimid
            savedict['values'] = self.ids[ct1:ct1+ln]
            f.add_data(savedict)
            

            f.close()
            
            ct1 += ln
            
            logging.debug("Successfully wrote data to obs file %s"%newfile)
            

################### End Class STILT ###################


if __name__ == "__main__":
    pass


